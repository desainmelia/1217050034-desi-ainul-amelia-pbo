# 1217050034-Desi Ainul Amelia-PBO

# No 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait).

Dalam pendekatan matematika, disini saya menggunakan operator logika p==q maka true, dimana diprogram yang saya buat, saya banyak membuat pilihan menggunakan algoritma pemrograman if else. yang mana ketika kondisi dan situasi yang sama akan berniali benar dan bisa lanjut ke step selanjutnya. 

Algoritma yang saya pakai adalah algoritma percabangan, algortima digunakan untuk memilih atau melanjutkan salah satu perintah, setiap melaju kepada step selanjutnya, kita harus memenuhi syarat yang telah ditentukan, yang dimana salah satu syarat yang saya gunakan adalah operator logika.

berikut contoh programnya.

        System.out.println("\n1. Watch");
        System.out.println("\n2. Download\n");
        int choice = input.nextInt();

        if (choice == 1) {
            playing.play();
            playing.pilihEpisode(episode);
            playing.pilihSubtittle(subtittle);
            System.out.println("\n***************************************************************");
            System.out.println("\nDrama Sudah Selesai, Apakah Kamu Ingin memberikan Rating? (Y/N)");
            char option = input.next().charAt(0);
            if (option == 'Y') {
                System.out.println("\nBerapa Rating yang ingin kamu berikan? (1-5)");
                int userRating = input.nextInt();
                review.setRating(userRating);
            }
            System.out.println("\nApakah Kamu ingin Mendownload Film ini? (Y/N)");
            char option1 = input.next().charAt(0);
            if (option1 == 'Y'){
                download.download();
            }
        } else if (choice == 2) {
            download.pilihEpisode(episode);
            download.pilihSubtittle(subtittle);
            download.download();
        } else {
            System.out.println("Pilihan-mu tidak tersedia");

# No 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
    Untuk mempermudah penjelasan saya melampirkan materi tentang operator logika

![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot__183_.png)

Jadi, ketika program saya berjalan dan harus memilih suatu kondisi yang di inginkan, juga karena saya mengimplementasikan operator logika p==q maka true. jadi ketika saya memilih untuk menonton saya harus menginputkan kondisi yang akan saya lakukan. dan akan dimunculkan aksi setelah kondisi dan situasi bernilai benar dua duanya.

# No 3
Mampu menjelaskan konsep dasar OOP

PBO/OOP merupakan suatu program yang berisi informasi dalam sebuah object/class 4 kata kunci OOP adalah 
- Class, yang merpakan rancangan suatu program. 
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot_2023-05-17_182430.png)
gambar ini menjelaskan bahwa ini merupakan contoh class yang memiliki rancangan dapat mengeluarkan suatu tampilan film yang memiliki judul, kategori dan tahun.

- Object yang mewujudkan rancangan atau mengeluarkan output dari class
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot_2023-05-17_182840.png)
pada gambar ini untuk mewujudkan suatu rancangan dari sebuah class, kita harus membuat object dalam pemanggilannya. disini saya menggunakan object film untuk melakukan pemanggilannya.

 - atribut, yang berisikan sebuah variable dan type data, bisa di lihat pada gambar yang saya lampirkan dipoint class disana terdapat atribut judul, kategori dan tahun yang memiliki type data String dan int untuk tahun.


- Methode merupakan suatu aksi yang dapat dilakukan oleh object, berikut contoh methode dalam program saya
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot_2023-05-17_183523.png) seperti suatu perintah yang harus dilakukan dan jika dilakukan pemanggilan contohnya seperti ini:
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot_2023-05-17_183742.png) karena object dari netfPlaying adalah playing maka ketika pemanggilan aksi yang akan dilakukan methodenya kita tuliskan terlebih dahulu nama objectnya kemudian nama methodenya disini saya memanggilnya speerti ini playing.play(). disclaimer ada juga methode yang tidak mengembalikan nilai.

Selanjutnya dalam oop pun terdapat 4 pilar OOP, yaitu Abstraction, Encapsulation, Inheritance dan Polymorphism.

- Abstraction adalah class yang memberikan gambaran umum, untuk memudahkan public memperoses suatu class, Abstraction juga hanya menggambarkan secara kasar suatu methode yang dimana nantinya akan dijelaskan ekseskusinya oleh kelas turunannya. bisa di lihat pada gambar contoh methode pada 4 kata kunci OOP, disana terdapat methode yang belum memiliki aksi.

- Encapsulation memberikan batasan terhadap suatu atribut agar tidak digunakan di sembarang class, pada gambar class diatas saya menggunakan limit akses private yang tidak bisa digunakan kelas lain, jika ingin menggunakannya harus menjadi salah satu turunan classnya. Encapsulation terdapat 3 akses yaitu, private, protected, dan public.

- Inheritance adalah suatu pewaris, dimana pada Inheritance terdapat super class dan subclass, atau biasa disebut parent and childern class, inheritance ini salah satu cara agar kita bisa mengakses, menjelaskan suatu atribut yang terdapat pada class abstract dan suatu atribut yang menggunakan akses private pada atributnya, akan tetapi suatu subclass dapat mempunyai atributnya sendiri.

- Polymorphism adalah kemiripan, dimana terdapat methode yang sama tapi memiliki aksi yang berbeda, contohnya seperti methode abstract play, dimana jika di netfplaying dan netfdownload pada program saya, tingkah lakunya masih sama, jadi yang menjadikan program saya melakukan Polymorphism adalah perbedaan aksi pada netfreview, dimana methode nya setelah menonton akan memberikan review.

# No 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

saya menggunakan pencapsulan privasi dalam setiap data yang akan di inputkan, berikut saya lampirkan souce code nya: 


    class regisUser{
            private String username;
            private String password;
            private String email;
            private String noTlp;
            private String metodePembayaran;
            private String paketLangganan;

        public regisUser(String username, String password, String email, String noTlp, String metodePembayaran, String paketLangganan) {
            this.username = username;
            this.password = password;
            this.email = email;
            this.noTlp = noTlp;
            this.metodePembayaran = metodePembayaran;
            this.paketLangganan = paketLangganan;
        }
        public String getUsername() {
            return username;
        }
        public void setUsername(){
            this.username = username;
        }

        public String getPassword() {
            return email;
        }
        public void setPassword(){
            this.password = password;
        }

        public String getEmail() {
            return email;
        }
        public void setEmail(){
            this.email = email;
        }

        public String getnoTlp() {
            return noTlp;
        }
        public void setnoTlp(){
            this.noTlp = noTlp;
        }

        public String getmetodePembayaran() {
            return metodePembayaran;
        }
        public void setmetodePembayaran(){
            this.metodePembayaran = metodePembayaran;
        }

        public String getpaketLangganan() {
            return paketLangganan;
        }
        public void setpaketLangganan(){
            this.paketLangganan = paketLangganan;
        }
    }

pada class regisUser ini saya menggunakan akses modif private pada atribut yang saya deklarasikan, untuk editing atributnya kita bisa menggunakan set and get, kenapa saya menggunakan set, untuk menyunting atau menambahkan sendiri dengan keyboard inputan yang saya inginkan, dan kenapa saya menggunakan get, karna saya menginginkan ada nya verifikasi data jadi data verifikasi tersebut diambil oleh get yang dimana data sebelumnya telah di set.


# No 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)

saya mengimplementasikan Abstraction pada class netfplay karna memiliki banyak turunan kelas yang memiliki atribut yang sama dan perilaku yang sama jadi untuk menggambarkannya saya menggunakan Abstraction berikut programnya

    abstract class netfPlay {
            private String judul;
            private String kategori;
            private int durasi;
            private String subtittle;
            private String fileVideo;
        
            public netfPlay(String judul, String kategori, int durasi, String fileVideo, String subtittle) {
                this.judul = judul;
                this.kategori = kategori;
                this.durasi = durasi;
                this.fileVideo = fileVideo;
                this.subtittle = subtittle;
            }
        
            public String getJudul() {
                return judul;
            }
        
            public void setJudul(String judul) {
                this.judul = judul;
            }
        
            public String getKategori() {
                return kategori;
            }
        
            public void setKategori(String kategori) {
                this.kategori = kategori;
            }
        
            public int getDurasi() {
                return durasi;
            }
        
            public void setDurasi(int durasi) {
                this.durasi = durasi;
            }
        
            public String getFileVideo() {
                return fileVideo;
            }
        
            public void setFileVideo(String fileVideo) {
                this.fileVideo = fileVideo;
            }
        
            public String getSubtittle() {
                return subtittle;
            }
        
            public void setSubtittle(String subtittle) {
                this.subtittle = subtittle;
            }
        
            public abstract void play();
        
            public abstract void setEpisode(int episode);
        
            public abstract void setSubtittle(String subtittle);
}

# No 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

Berikut adalah contoh program Inheritance dan Polymorphism

    class netfPlaying extends netfPlay {
            private int episode;
            private String subtittle;
        
            public netfPlaying(String judul, String kategori, int durasi, String fileVideo, int episode, String subtittle) {
                super(judul, kategori, durasi, fileVideo, subtittle);
                this.episode = episode;
                this.subtittle = subtittle;
            }
        
            public int getEpisode() {
                return episode;
            }
        
            public void setEpisode(int episode) {
                this.episode = episode;
            }
        
            public String getSubtittle() {
                return subtittle;
            }
        
            public void setSubtittle(String subtittle) {
                this.subtittle = subtittle;
            }
        
            @Override
            public void play() {
                System.out.println("\nKamu Sedang Menonton Episode " + episode + " Drama " + getJudul() + " Dengan Subtittle " + subtittle);
            }
        
            public void settingEpisode(int episode) {
                this.episode = episode;
                System.out.println("Episode " + episode + " Drama " + getJudul());
            }
        
            public void settingSubtittle(String subtittle) {
                this.subtittle = subtittle;
                System.out.println("Memilih " + subtittle + " Subtittle dari " + getJudul());
            }

berikut contoh Polymorphismnya
    
    class netfReview extends netfPlayer {
            private int rating;
        
            public netfReview(String judul, String kategori, int durasi, String fileVideo, String subtittle, int rating) {
                super(judul, kategori, durasi, fileVideo, subtittle);
                this.rating = rating;
            }
        
            public int getRating() {
                return rating;
            }
        
            public void setRating(int rating) {
                this.rating = rating;
            }
        
            @Override
            public void play() {
                super.play();
                System.out.println("\nKamu Mereview " + getJudul() + " dengan rating " + rating);
            }
        
            @Override
            public void download() {
                System.out.println("Waktunya Mereview");
            }
        }


inheritance pada dua class ini mengextends dari class abstract netfPlay, lalu polymorphismnya adalah ketika perbedaan aksi yang dilakukan methode pada class netfPlaying dan netfreview.

# No 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

- pada proses regis dan login konsep oop yang saya implementasikan adalah Encapsulation menggunakan akses modif private dan get and set dalam rancangannya, agar dapat memasukan data dari keyboard pengguna maka saya gunakan set dalam pengaksesannyam lalu getter untuk memberikan verifikasi data dan keberhasilan langganan.

- selanjutnya pada tampilan daftar film saya menggunakan encaptulation juga agar tetap terkapsul rapih, disini saya memanggil class netfmovie untuk akses constructornya yang terdapat di dalamnya dan saya memakai arraylist dalam menampilkan daftar filmnya.

- tidak jauh berbeda dengan daftar film, disini juga saya memakai encaptulation juga untuk menjaga list filmnya, dan mengambil class netfmovie juga untuk akses constructornya dalam mendeklarasikan judul filmnya.

- pada pemilihhan menonton dan mendownload film saya pakai pemanggilan methode menggunakan pembuatan object sebelum memanggil methodenya, pada class ini saya mengimplementasikan 4 pilar oop didalamnya untuk menggambarkan secara umum saya masukan methode yang sama dan atribut yang sama sampai bisa send review terhadap film yang akan di tonton.

- selanjutnya pada class netfdownload netfPlaying san netfReview saya memanggil object dan methodenya. perilaku yang signifikan berbeda terdapat pada netfreview karena perbedaan perilaku yang merupakan implementasi dari polymorphismnya.



# No 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

class Diagram
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Untitled_Diagram-Page-4.drawio__1_.png)

pada class diagram ini, saya mengambil dari beberapa hal yang diprioritaskan, dimana nantinya setelah registrasi akan tampil verifikasi data dan suksenya payment, lalu untuk mengakses banyak film dan menontonnya harus bergantung pada proses loginnya, setelah itu ada inheritance yang dilakukan abstract class netfPlay kepada netfPlaying netfdownload dan netfReview.

usecase 
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot_2023-05-17_192117.png)

pada usecase ini saya memprioritaskan dapat melakukan registrasi dan login pada netflix website, lalu melihat daftar film setelah login, selanjutnya mencari film dan memilih untuk menontonnya lalu dapat mendownload film tersebut dan memberikan review pada film yang telah selesai ditonton.


# No 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

https://youtu.be/r0y0zQfBOtw

# No 10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot__188_.png)

![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot__189_.png)

![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-pbo/-/raw/main/Dokumentasi/Screenshot__190_.png)

